<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Podcast;

/**
 * PodcastSearch represents the model behind the search form about `app\models\Podcast`.
 */
class PodcastSearch extends Podcast
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'artist_id', 'audio_file_length'], 'integer'],
            [['guid', 'artist', 'title', 'episode', 'pub_date', 'link', 'description', 'audio_file_url', 'audio_file_type', 'duration', 'author', 'summary', 'subtitle', 'image', 'date_modified', 'date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Podcast::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'artist_id' => $this->artist_id,
            'audio_file_length' => $this->audio_file_length,
            'duration' => $this->duration,
            'date_modified' => $this->date_modified,
            'date_created' => $this->date_created,
        ]);

        $query->andFilterWhere(['like', 'guid', $this->guid])
            ->andFilterWhere(['like', 'artist', $this->artist])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'episode', $this->episode])
            ->andFilterWhere(['like', 'pub_date', $this->pub_date])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'audio_file_url', $this->audio_file_url])
            ->andFilterWhere(['like', 'audio_file_type', $this->audio_file_type])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'summary', $this->summary])
            ->andFilterWhere(['like', 'subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
