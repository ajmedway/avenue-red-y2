<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "podcast".
 *
 * @property integer $id
 * @property string $guid
 * @property string $artist
 * @property integer $artist_id
 * @property string $title
 * @property string $episode
 * @property string $pub_date
 * @property string $link
 * @property string $description
 * @property string $audio_file_url
 * @property string $audio_file_type
 * @property integer $audio_file_length
 * @property string $duration
 * @property string $author
 * @property string $summary
 * @property string $subtitle
 * @property string $image
 * @property string $date_modified
 * @property string $date_created
 */
class Podcast extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'podcast';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guid'], 'required'],
            [['artist_id', 'audio_file_length'], 'integer'],
            [['description', 'summary'], 'string'],
            [['duration', 'date_modified', 'date_created'], 'safe'],
            [['guid', 'artist', 'title', 'episode', 'pub_date', 'link', 'audio_file_url', 'audio_file_type', 'author', 'subtitle', 'image'], 'string', 'max' => 255],
            [['guid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guid' => 'Guid',
            'artist' => 'Artist',
            'artist_id' => 'Artist ID',
            'title' => 'Title',
            'episode' => 'Episode',
            'pub_date' => 'Pub Date',
            'link' => 'Link',
            'description' => 'Description',
            'audio_file_url' => 'Audio File Url',
            'audio_file_type' => 'Audio File Type',
            'audio_file_length' => 'Audio File Length',
            'duration' => 'Duration',
            'author' => 'Author',
            'summary' => 'Summary',
            'subtitle' => 'Subtitle',
            'image' => 'Image',
            'date_modified' => 'Date Modified',
            'date_created' => 'Date Created',
        ];
    }
    
        /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // handle created/modified timestamps
            $this->date_modified = new \yii\db\Expression('NOW()');
            if ($insert) {
                $this->date_created = new \yii\db\Expression('NOW()');
            }
            return true;
        } else {
            return false;
        }
    }
}
