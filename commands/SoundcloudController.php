<?php

/**
 * Copyright © 2019. All rights reserved.
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Inflector;
use app\models\Podcast;

/**
 * This class contains console command actions relating to Soundcloud data.
 */
class SoundcloudController extends Controller
{

    /**
     * This command loads and parses data from the Soundcloud RSS feed, which is
     * used to create and update Podcast records.
     * 
     * Command: ./yii soundcloud/import-podcasts-from-rss [true|false]
     * 
     * @todo Add field to store md5 hash of parsed podcast item to detect any
     * changes in the source, which would enable to determine if a podcast
     * requires updating.
     * 
     * @param bool $dryRun If the script should execute inserts/updates.
     * @return string The completion message to be echoed.
     */
    public function actionImportPodcastsFromRss($dryRun = false)
    {
        error_reporting(E_ALL);
        libxml_use_internal_errors(true);

        // get raw rss feed xml data
        $feedUrl = 'http://feeds.soundcloud.com/users/soundcloud:users:51348514/sounds.rss';
        $feedXml = file_get_contents($feedUrl);

        // parse xml string as SimpleXMLElement object to interrogate data
        $feed = simplexml_load_string($feedXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        // if there is a problem with the source xml, display any errors and exit
        if ($feed === false) {
            // explode the raw feed xml on newlines so that we can refer to line numbers on error
            $xmlDataArray = explode("\n", $feedXml);
            $errors = libxml_get_errors();

            foreach ($errors as $error) {
                Yii::error(['message' => $this->_get_xml_error($error, $xmlDataArray)], __METHOD__);
            }

            libxml_clear_errors();
            exit;
        }

        // get array of namespaces used in the xml document
        $nsdu = $feed->getNamespaces(true);

        // initialise empty container for podcast items
        $podcastItems = [];

        foreach ($feed->channel->item as $itemIdx => $item) {

            // instantiate new item comtainer
            $itemData = new \stdClass;

            // iterate through used namespaces, then hunt for children in each namespace
            foreach (array_merge([null], $nsdu) as $nsk => $ns) {

                // find children of the given node/namespace, then iterate
                $nsDataChildren = empty($nsk) ? $item->children() : $item->children($nsk, true);

                foreach ($nsDataChildren as $nodeName => $child) {

                    $nodeNameId = Inflector::camel2id($nodeName, '_');

                    $attributes = $child->attributes();
                    $itemData->{$nodeNameId} = new \stdClass;

                    // get properties from xml attributes where necessary
                    if (in_array($nodeName, ['image'])) {

                        $itemData->{$nodeNameId} = $attributes->href->__toString();
                    } /* elseif (in_array($nodeName, ['pubDate'])) {

                      //$timestamp = strtotime($child->__toString());
                      $itemData->{$nodeNameId} = $child->__toString();

                      } */ elseif (in_array($nodeName, ['title'])) {

                        // add custom episode/artist fields
                        $title = $child->__toString();
                        $titleParts = explode(' - ', $title);
                        $episode = reset($titleParts);
                        $artist = end($titleParts);

                        $itemData->episode = trim($episode);

                        // filter out anything after the @ symbol, which demarks live event details
                        $artist = strtok($artist, '@');
                        $itemData->artist = trim($artist);

                        // map string value directly to the child node name
                        $itemData->{$nodeNameId} = $child->__toString();
                    } elseif ($nodeName === 'enclosure' && $attributes->count() > 0) {

                        // ...also get the attributes
                        foreach ($attributes as $attribute => $attributeValue) {
                            $itemData->{"audio_file_{$attribute}"} = $attributeValue->__toString();
                        }
                        unset($itemData->enclosure);
                    } elseif (!in_array($nodeName, ['guid']) && $attributes->count() > 0) {

                        // get node value if exists
                        if ($child->__toString()) {
                            $itemData->{$nodeNameId}->value = $child->__toString();
                        }

                        // ...also get the attributes
                        foreach ($attributes as $attribute => $attributeValue) {
                            $itemData->{$nodeNameId}->{$attribute} = $attributeValue->__toString();
                        }
                    } else {

                        // map string value directly to the child node name
                        $itemData->{$nodeNameId} = $child->__toString();
                    }
                }
            }

            // add the fully built podcast item
            $podcastItems[] = $itemData;
        }

        // reverse the order of the array so that podcast #1 is first, most recent last for updates/inserts
        $podcastItems = array_reverse($podcastItems);

        // show the items to be parsed and exit the script
        if ($dryRun) {
            $completionMessage = '$podcastItems: ' . print_r($podcastItems, true) . PHP_EOL . 'DONE.' . PHP_EOL;
            exit($completionMessage);
        }

        $totalSavedUpdated = 0;

        // create/update entities from the feed based on the soundcloud guid
        foreach ($podcastItems as $podcastItem) {
            if (!empty($podcastItem->guid)) {
                $podcast = Podcast::find()
                    ->where(['guid' => $podcastItem->guid])
                    ->one();

                if (empty($podcast)) {
                    // doesn't exist, so create a new one
                    $podcast = new Podcast();
                }

                // prepare the data in the correct format for furnishing the model
                $podcastData = ['Podcast' => (array) $podcastItem];

                if ($podcast->load($podcastData) && $podcast->save()) {
                    $totalSavedUpdated++;
                } else {
                    Yii::warning(['$podcast->getErrors()' => $podcast->getErrors()], __METHOD__);
                }
            }
        }

        // finalise with info logging and exit message
        $completionMessage = "Created/updated {$totalSavedUpdated} Podcast records." . PHP_EOL . 'DONE.' . PHP_EOL;
        Yii::info(['message' => $completionMessage], __METHOD__);
        exit($completionMessage);
    }

    private function _get_xml_error($error, $xml)
    {
        $return = $xml[$error->line - 1] . "\n";
        $return .= str_repeat('-', $error->column) . "^\n";

        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }

        $return .= trim($error->message) .
            "\n  Line: $error->line" .
            "\n  Column: $error->column";

        if ($error->file) {
            $return .= "\n  File: $error->file";
        }

        return "$return\n\n--------------------------------------------\n\n";
    }

}
