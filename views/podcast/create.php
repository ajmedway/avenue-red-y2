<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Podcast */

$this->title = 'Create Podcast';
$this->params['breadcrumbs'][] = ['label' => 'Podcast', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="podcast-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
