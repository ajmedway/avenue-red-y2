<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PodcastSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Podcast';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="podcast-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Podcast', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'guid',
            'image:image',
            'artist',
            // 'artist_id',
            'title',
            'episode',
            // 'pub_date',
            'link',
            // 'description:ntext',
            // 'audio_file_url:url',
            // 'audio_file_type',
            // 'audio_file_length',
            // 'duration',
            // 'author',
            // 'summary:ntext',
            'subtitle',
            // 'date_modified',
            // 'date_created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
