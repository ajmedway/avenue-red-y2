<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Podcast */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Podcast', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="podcast-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'guid',
            'pub_date',
            'title',
            'episode',
            'image:image',
            'artist',
            'artist_id',
            'subtitle',
            'link',
            'duration',
            'description:ntext',
            'audio_file_url:url',
            'audio_file_type',
            'audio_file_length',
            'author',
            //'summary:ntext',
            'date_modified',
            'date_created',
        ],
    ]) ?>

</div>
