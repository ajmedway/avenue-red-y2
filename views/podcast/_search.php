<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PodcastSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="podcast-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'guid') ?>

    <?= $form->field($model, 'artist') ?>

    <?= $form->field($model, 'artist_id') ?>

    <?= $form->field($model, 'title') ?>
    
    <?= $form->field($model, 'episode') ?>

    <?php // echo $form->field($model, 'pub_date') ?>

    <?php // echo $form->field($model, 'link') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'audio_file_url') ?>

    <?php // echo $form->field($model, 'audio_file_type') ?>

    <?php // echo $form->field($model, 'audio_file_length') ?>

    <?php // echo $form->field($model, 'duration') ?>

    <?php // echo $form->field($model, 'author') ?>

    <?php // echo $form->field($model, 'summary') ?>

    <?php // echo $form->field($model, 'subtitle') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'date_modified') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
